#include <unistd.h>
#include <stdio.h>
#include <xcb/xcb.h>

int main() {
  xcb_connection_t *c;
  xcb_screen_t *screen;
  int screen_number;
  xcb_screen_iterator_t iter;
  xcb_window_t win;
  xcb_pixmap_t pixmap;
  xcb_gcontext_t graphic_context;
  xcb_drawable_t drawable;
  uint32_t value[1];

  c = xcb_connect(NULL, &screen_number);

  screen = xcb_setup_roots_iterator(xcb_get_setup(c)).data;

  win = xcb_generate_id(c);
  pixmap = xcb_generate_id(c);
  graphic_context = xcb_generate_id(c);

  value[0] = screen->black_pixel;
  xcb_create_gc(c,
                graphic_context,
                drawable,
                XCB_GC_FOREGROUND,
                value);

  xcb_create_window(c,
                    XCB_COPY_FROM_PARENT,
                    win,
                    screen->root,
                    0, 0,
                    150, 150,
                    10,
                    XCB_WINDOW_CLASS_INPUT_OUTPUT,
                    screen->root_visual,
                    0, NULL);

  /* xcb_create_pixmap(c, */
  /*                   source_drawable, dest_drawable, */
  /*                   graphic_context, */
  /*                   source_top_left_x, source_top_left_y, */
  /*                   dest_top_left_x, dest_top_left_y, */
  /*                   width, height); */

  xcb_map_window(c, win);

  xcb_flush(c);

  printf("\n");
  printf("Information of screen %ld:\n", screen->root);
  printf("  width..........: %d\n", screen->width_in_pixels);
  printf("  height.........: %d\n", screen->height_in_pixels);
  printf("  white pixel....: %ld\n", screen->white_pixel);
  printf("  black pixel....: %ld\n", screen->black_pixel);
  printf("\n");

  pause();

  /* xcb_free_pixmap(c, pixmap); */
  xcb_disconnect(c);
  return 0;
}
