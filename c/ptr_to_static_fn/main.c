#include <stdio.h>
#include <stdlib.h>
#include "library.c"

int main(int Argc, char *Argv[])
{
  int Arg1, Arg2;

  if (Argc != 3) {
    printf("Usage: ./a.out arg1 arg2\n");
    return 0;
  }
  else {
    Arg1 = atoi(Argv[1]);
    Arg2 = atoi(Argv[2]);
  }

  my_add_ptr Adder = GetPointerToPrivateFunction();
  printf("Adder(%d, %d): %d\n", Arg1, Arg2, Adder(Arg1, Arg2));
  return 0;
}
