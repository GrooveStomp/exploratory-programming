#define global
#define file_local static
#define local_persist static

/* Defines a new type: 'my_add_ptr' that is a pointer to a function returning an
   int, which takes two integer parameters. */
typedef int (*my_add_ptr)(int, int);

/* File-local (ie., private) function adding two integers. */
file_local
int
MyAdd(int Arg1, int Arg2)
{
  Arg1 = Arg1 + Arg2;
  return Arg1;
}

/* Returns a pointer to a function matching the prototype of 'MyAdd'. */
global
my_add_ptr
GetPointerToPrivateFunction(void)
{
  return MyAdd;
}
