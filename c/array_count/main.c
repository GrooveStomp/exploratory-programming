#include <stdio.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

struct Vector {
  int X;
  int Y;
  int Z;
};

int main()
{
  int Numbers[] = { 1, 2, 3, 4, 5, 6 };
  printf("Number of elements in Numbers: '%d'\n", ArrayCount(Numbers));

  struct Vector Vectors[] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
  printf("Number of elements in Vectors: '%d'\n", ArrayCount(Vectors));

  return 0;
}
