#include <stdio.h>

typedef int element;

struct html_elements {
  element Header;
  element Sidebar;
  element Content;
  element Footer;
};

struct html_elements Html(element, element, element, element);
element SectionLabel(char *);
element Link(char *);
element Header(char *);
element Sidebar(char *);
element Content(char *);
element Footer(char *);

int main(int argc, char *argv[])
{
  struct html_elements Output = Html(
    Header("GrooveStomp.com"),
    Sidebar("Sidebar"),
    Content("Content"),
    Footer("Footer")
  );

  printf("%s\n", Output);

  return 0;
}

struct html_elements Html(element Header, element Sidebar, element Content, element Footer)
{
  struct html_elements Result = {Header, Sidebar, Content, Footer};
  return Result;
}

element SectionLabel(char *Text)
{
  return 0;
}

element Link(char *Text)
{
  return 0;
}

element Header(char *Text)
{
  return 0;
}

element Sidebar(char *Text)
{
  /* return Array( */
  /*   SectionLabel("..."), */
  /*   Link("..."), */
  /*   Link("..."), */
  /*   Link("..."), */
  /*   SectionLabel("..."), */
  /*   Link("..."), */
  /*   Link("...") */
  /* ); */
  return 0;
}

element Content(char *Text)
{
  return 0;
}

element Footer(char *Text)
{
  return 0;
}
